<?
$MESS['SALE_HPS_TIPTOPPAY'] = 'TipTop Pay';
$MESS["SALE_HPS_TIPTOPPAY_SHOP_ID"] = "Public ID";
$MESS["SALE_HPS_TIPTOPPAY_SHOP_ID_DESC"] = "Access key (from the back-office TipTop Pay)";
$MESS["SALE_HPS_TIPTOPPAY_SHOP_KEY"] = "Password for the API";
$MESS["SALE_HPS_TIPTOPPAY_SHOP_KEY_DESC"] = "Access password (from the back-office TipTop Pay )";
$MESS["SALE_HPS_TIPTOPPAY_CHECKONLINE"] = "Use online cash register functionality";
$MESS["SALE_HPS_TIPTOPPAY_CHECKONLINE_DESC"] = "This functionality should be enabled on the TipTop Pay side";
$MESS["SALE_HPS_TIPTOPPAY_PAYMENT_TYPE"] = "Payment system type";
$MESS["SALE_HPS_TIPTOPPAY_CURRENCY"] = "Order currency";
$MESS["SALE_HPS_TIPTOPPAY_INN"] = "TIN of the organization";
$MESS["SALE_HPS_TIPTOPPAY_INN_DESC"] = "TIN of your organization or PI for which the cash register is registered";
$MESS["SALE_HPS_TIPTOPPAY_TYPE_NALOG"] = 'Type of taxation system';
$MESS["SALE_HPS_TIPTOPPAY_TYPE_NALOG_DESC"] = 'This tax system must match one of the options registered in the cash register.';
$MESS["SALE_HPS_TIPTOPPAY_TYPE_NALOG"] = 'Type of taxation system';
$MESS["SALE_HPS_TIPTOPPAY_TYPE_NALOG_DESC"] = 'This tax system must match one of the options registered in the cash register.';
$MESS["SALE_HPS_NALOG_TYPE_0"] = "General tax regime";
$MESS["SALE_HPS_NALOG_TYPE_1"] = "CPR based on simplified declaration";
$MESS["SALE_HPS_NALOG_TYPE_4"] = "CPR based on patent";
$MESS["VBCH_CLPAY_SPCP_DDESCR"] = "<a href=\"http://www.http://tiptoppay.ru/\"> TipTopPay </a>. <br> Receiving payments online using a bank card through the system TipTopPay <Br/>
Go to the back-office TipTopPay  and change the following paths: <br/>
Settings Notifications notification: http://".$_SERVER['HTTP_HOST']."/index.php?option=com_hikashop&ctrl=checkout&task=notify&notif_payment=tiptoppay&tmpl=component&lang=ru&action=check<br/>
Pay notification settings: http://".$_SERVER['HTTP_HOST']."/index.php?option=com_hikashop&ctrl=checkout&task=notify&notif_payment=tiptoppay&tmpl=component&lang=ru&action=pay<br/>
Fail notification settings: http://".$_SERVER['HTTP_HOST']."/index.php?option=com_hikashop&ctrl=checkout&task=notify&notif_payment=tiptoppay&tmpl=component&lang=ru&action=fail<br/>
Cancel notification settings: http://".$_SERVER['HTTP_HOST']."/index.php?option=com_hikashop&ctrl=checkout&task=notify&notif_payment=tiptoppay&tmpl=component&lang=ru&action=cancel<br/>
Confirm notification settings: http://".$_SERVER['HTTP_HOST']."/index.php?option=com_hikashop&ctrl=checkout&task=notify&notif_payment=tiptoppay&tmpl=component&lang=ru&action=confirm<br/>
Refund notification settings: http://".$_SERVER['HTTP_HOST']."/index.php?option=com_hikashop&ctrl=checkout&task=notify&notif_payment=tiptoppay&tmpl=component&lang=ru&action=refund<br/><br/>";

$MESS["SALE_HPS_TIPTOPPAY_TYPE_SYSTEM"] = "Payment scheme type";
$MESS["SALE_HPS_TYPE_SCHEME_0"] = "One-Step Payment";
$MESS["SALE_HPS_TYPE_SCHEME_1"] = "Two-Step Payment";

$MESS["SALE_HPS_TIPTOPPAY_SUCCESS_URL"] = "Success URL";
$MESS["SALE_HPS_TIPTOPPAY_SUCCESS_URL_DESC"] = "";
$MESS["SALE_HPS_TIPTOPPAY_FAIL_URL"] = "Fail URL";
$MESS["SALE_HPS_TIPTOPPAY_FAIL_URL_DESC"] = "";
$MESS["SALE_HPS_TIPTOPPAY_WIDGET_LANG"] = "Widget Language";
$MESS["SALE_HPS_TIPTOPPAY_WIDGET_LANG_DESC"] = "";

$MESS["SALE_HPS_WIDGET_LANG_TYPE_0"] = "Russian MSK";
$MESS["SALE_HPS_WIDGET_LANG_TYPE_1"] = "English CET";
$MESS["SALE_HPS_WIDGET_LANG_TYPE_2"] = "Latvian CET";
$MESS["SALE_HPS_WIDGET_LANG_TYPE_3"] = "Azerbaijani AZT";
$MESS["SALE_HPS_WIDGET_LANG_TYPE_4"] = "Russian ALMT";
$MESS["SALE_HPS_WIDGET_LANG_TYPE_5"] = "Kazakh ALMT";
$MESS["SALE_HPS_WIDGET_LANG_TYPE_6"] = "Ukrainian EET";
$MESS["SALE_HPS_WIDGET_LANG_TYPE_7"] = "Polish CET";
$MESS["SALE_HPS_WIDGET_LANG_TYPE_8"] = "Portuguese CET";
$MESS["SALE_HPS_WIDGET_LANG_TYPE_9"] = "Czech CET";

$MESS["SALE_HPS_TIPTOPPAY_VAT_DELIVERY"] = "Select Shipping VAT, if necessary";
$MESS["SALE_HPS_TIPTOPPAY_VAT_DELIVERY_DESC"] = "";

$MESS["VAT"] = "Select VAT for shipping if necessary";
$MESS["NOT_VAT"] = "Without VAT";

$MESS["STATUS_AU"] = "Authorized status";
$MESS["STATUS_VOID"] = "Canceled status";
$MESS["STATUS_GROUP"] = "Statuses";
$MESS["STATUS_GROUP"] = "Statuses";
$MESS["STATUS_PAY"] = "Paid status";
$MESS["STATUS_CHANCEL"] = "Refund Status";
$MESS["STATUS_AUTHORIZE"]= "Сanceled order(two-stage payments)";

$MESS["SALE_HPS_TIPTOPPAY_WIDGET_SKIN"]="Widget design";
$MESS["SALE_HPS_WIDGET_SKIN_0"]="classic";
$MESS["SALE_HPS_WIDGET_SKIN_1"]="modern";
$MESS["SALE_HPS_WIDGET_SKIN_2"]="mini";

$MESS["RUB"] = "The Russian Ruble";
$MESS["EUR"] = "Euro";
$MESS["USD"] = "US Dollar";
$MESS["GBP"] = "Pound Sterling";
$MESS["UAH"] = "Ukrainian Hryvnia";
$MESS["BYR"] = "Belarusian ruble (not used since July 1, 2016)";
$MESS["BYN"] = "The Belarusian Ruble";
$MESS["KZT"] = "Kazakh tenge";
$MESS["AZN"] = "Azerbaijani manat";
$MESS["CHF"] = "Swiss franc";
$MESS["CZK"] = "The Czech crown";
$MESS["CAD"] = "Canadian Dollar";
$MESS["PLN"] = "Polish zloty";
$MESS["SEK"] = "Swedish Krona";
$MESS["TRY"] = "Turkish Lira";
$MESS["CNY"] = "Chinese yuan";
$MESS["INR"] = "Indian Rupee";
$MESS["BRL"] = "Brazilian Real";
$MESS["ZAL"] = "South African Rand";
$MESS["UZS"] = "Uzbek Sum";
$MESS["BGL"] = "Bulgarian Lev";

$MESS["SALE_HPS_TIPTOPPAY_NDS"] = "VAT for the order";
$MESS["SALE_HPS_TIPTOPPAY_NDS_DELIVERY"] = "Shipping VAT";

$MESS["SALE_HPS_NDS"] = "Without VAT";
$MESS["SALE_HPS_NDS_0"] = "VAT 0%";
$MESS["SALE_HPS_NDS_10"] = "VAT 10%";
$MESS["SALE_HPS_NDS_12"] = "VAT 12%";
?>