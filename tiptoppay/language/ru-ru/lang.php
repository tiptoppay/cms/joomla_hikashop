<?
$MESS['SALE_HPS_TIPTOPPAY'] = 'TipTop Pay';
$MESS["SALE_HPS_TIPTOPPAY_SHOP_ID"] = "Public ID";
$MESS["SALE_HPS_TIPTOPPAY_SHOP_ID_DESC"] = "Ключ доступа (из личного кабинета TipTop Pay)";
$MESS["SALE_HPS_TIPTOPPAY_SHOP_KEY"] = "Пароль для API";
$MESS["SALE_HPS_TIPTOPPAY_SHOP_KEY_DESC"] = "Пароль доступа (из личного кабинета TipTop Pay)";
$MESS["SALE_HPS_TIPTOPPAY_CHECKONLINE"]="Использовать функционал онлайн касс";
$MESS["SALE_HPS_TIPTOPPAY_CHECKONLINE_DESC"]="Данный функционал должен быть включен на стороне TipTop Pay";
$MESS["SALE_HPS_TIPTOPPAY_PAYMENT_TYPE"] = "Тип платёжной системы";
$MESS["SALE_HPS_TIPTOPPAY_CURRENCY"]="Валюта заказа";
$MESS["SALE_HPS_TIPTOPPAY_INN"]="ИИН организации";
$MESS["SALE_HPS_TIPTOPPAY_INN_DESC"]="ИИН вашей организации или ИП, на который зарегистрирована касса";
$MESS["SALE_HPS_TIPTOPPAY_TYPE_NALOG"]='Тип системы налогообложения';
$MESS["SALE_HPS_TIPTOPPAY_TYPE_NALOG_DESC"]='Указанная система налогообложения должна совпадать с одним из вариантов, зарегистрированных в ККТ.';
$MESS["SALE_HPS_TIPTOPPAY_calculationPlace"]='Место осуществления расчёта';
$MESS["SALE_HPS_TIPTOPPAY_calculationPlace_DESC"]='по умолчанию берется значение из кассы';
$MESS["SALE_HPS_NALOG_TYPE_0"]="Общеустановленный режим налогообложения";
$MESS["SALE_HPS_NALOG_TYPE_1"]="СНР на основе упрощенной декларации";
$MESS["SALE_HPS_NALOG_TYPE_4"]="СНР на основе патента";
$MESS["VBCH_CLPAY_SPCP_DDESCR"] = "<a href=\"http://http://tiptoppay.ru/\">TipTop Pay</a>.<br>Приём платежей онлайн с помощью банковской карты через систему TipTop Pay <Br/>
Зайти в личный кабинет TipTop Pay и исправить пути: <br/>
&nbsp;&nbsp; 	Настройки Сheck уведомлений: http://".$_SERVER['HTTP_HOST']."/index.php? option=com_hikashop&ctrl=checkout&task=notify&amp;notif_payment=tiptoppay&tmpl=component&lang=ru&action=check<br/>
&nbsp;&nbsp;    Настройки Pay уведомлений: http://".$_SERVER['HTTP_HOST']."/index.php? option=com_hikashop&ctrl=checkout&task=notify&amp;notif_payment=tiptoppay&tmpl=component&lang=ru&action=pay<br/>
&nbsp;&nbsp;	Настройки Fail уведомлений: http://".$_SERVER['HTTP_HOST']."/index.php? option=com_hikashop&ctrl=checkout&task=notify&amp;notif_payment=tiptoppay&tmpl=component&lang=ru&action=fail<br/>
&nbsp;&nbsp;	Настройки Cancel уведомлений: http://".$_SERVER['HTTP_HOST']."/index.php? option=com_hikashop&ctrl=checkout&task=notify&amp;notif_payment=tiptoppay&tmpl=component&lang=ru&action=cancel<br/>
&nbsp;&nbsp;	Настройки Confirm уведомлений: http://".$_SERVER['HTTP_HOST']."/index.php? option=com_hikashop&ctrl=checkout&task=notify&amp;notif_payment=tiptoppay&tmpl=component&lang=ru&action=confirm<br/>
&nbsp;&nbsp;	Настройки Refund уведомлений: http://".$_SERVER['HTTP_HOST']."/index.php?option=com_hikashop&ctrl=checkout&task=notify&amp;notif_payment=tiptoppay&tmpl=component&lang=ru&action=refund<br/><br/>";

$MESS["SALE_HPS_TIPTOPPAY_TYPE_SYSTEM"] = "Тип схемы проведения платежей";
$MESS["SALE_HPS_TYPE_SCHEME_0"]="Одностадийная оплата";
$MESS["SALE_HPS_TYPE_SCHEME_1"]="Двухстадийная оплата";

$MESS["SALE_HPS_TIPTOPPAY_SUCCESS_URL"]="Success URL";
$MESS["SALE_HPS_TIPTOPPAY_SUCCESS_URL_DESC"]="";
$MESS["SALE_HPS_TIPTOPPAY_FAIL_URL"]="Fail URL";
$MESS["SALE_HPS_TIPTOPPAY_FAIL_URL_DESC"]="";
$MESS["SALE_HPS_TIPTOPPAY_WIDGET_LANG"]="Язык виджета";
$MESS["SALE_HPS_TIPTOPPAY_WIDGET_SKIN"]="Дизайн виджета";
$MESS["SALE_HPS_TIPTOPPAY_WIDGET_LANG_DESC"]="";


$MESS["SALE_HPS_WIDGET_SKIN_0"]="classic";	
$MESS["SALE_HPS_WIDGET_SKIN_1"]="modern";	
$MESS["SALE_HPS_WIDGET_SKIN_2"]="mini";


$MESS["SALE_HPS_WIDGET_LANG_TYPE_0"]="Русский MSK";	
$MESS["SALE_HPS_WIDGET_LANG_TYPE_1"]="Английский CET";	
$MESS["SALE_HPS_WIDGET_LANG_TYPE_2"]="Латышский CET";	
$MESS["SALE_HPS_WIDGET_LANG_TYPE_3"]="Азербайджанский AZT";	
$MESS["SALE_HPS_WIDGET_LANG_TYPE_4"]="Русский ALMT";
$MESS["SALE_HPS_WIDGET_LANG_TYPE_5"]="Казахский ALMT";	
$MESS["SALE_HPS_WIDGET_LANG_TYPE_6"]="Украинский EET";
$MESS["SALE_HPS_WIDGET_LANG_TYPE_7"]="Польский CET";	
$MESS["SALE_HPS_WIDGET_LANG_TYPE_8"]="Португальский CET";
$MESS["SALE_HPS_WIDGET_LANG_TYPE_9"]="Чешский CET";	


$MESS["SALE_HPS_TIPTOPPAY_VAT_DELIVERY"]="Выберите НДС на доставку, если необходимо";
$MESS["SALE_HPS_TIPTOPPAY_VAT_DELIVERY_DESC"]="";

$MESS["VAT"]="Выберите НДС на доставку, если необходимо";
$MESS["NOT_VAT"]="Без НДС";

$MESS["STATUS_GROUP"]="Статусы";
$MESS["STATUS_PAY"]="Статус оплачен";
$MESS["STATUS_CHANCEL"]="Статус возврата платежа";
$MESS["STATUS_AUTHORIZE"]="Статус подтверждения авторизации платежа (двухстадийные платежи)";
$MESS["STATUS_AU"]="Статус авторизованного платежа (двухстадийные платежи)";

$MESS["STATUS_VOID"]="Статус отмена авторизованного платежа (двухстадийные платежи)";


$MESS["RUB"]="Российский рубль";
$MESS["EUR"]="Евро";
$MESS["USD"]="Доллар США";
$MESS["GBP"]="Фунт стерлингов";
$MESS["UAH"]="Украинская гривна";
$MESS["BYR"]="Белорусский рубль (не используется с 1 июля 2016)";
$MESS["BYN"]="Белорусский рубль";
$MESS["KZT"]="Казахский тенге";
$MESS["AZN"]="Азербайджанский манат";
$MESS["CHF"]="Швейцарский франк";
$MESS["CZK"]="Чешская крона";
$MESS["CAD"]="Канадский доллар";
$MESS["PLN"]="Польский злотый";
$MESS["SEK"]="Шведская крона";
$MESS["TRY"]="Турецкая лира";
$MESS["CNY"]="Китайский юань";
$MESS["INR"]="Индийская рупия";
$MESS["BRL"]="Бразильский реал";
$MESS["ZAR"]="Южноафриканский рэнд";
$MESS["UZS"]="Узбекский сум";
$MESS["BGL"]="Болгарский лев";

$MESS["SALE_HPS_TIPTOPPAY_NDS"]="НДС для заказа";
$MESS["SALE_HPS_TIPTOPPAY_NDS_DELIVERY"]="НДС для доставки";

$MESS["SALE_HPS_NDS"]="Без НДС";
$MESS["SALE_HPS_NDS_0"]="НДС 0%";
$MESS["SALE_HPS_NDS_10"]="НДС 10%";
$MESS["SALE_HPS_NDS_12"]="НДС 12%";
?>