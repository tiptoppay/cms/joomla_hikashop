# TipTop Pay модуль для Joomla - HikaShop
Модуль позволит с легкостью добавить на ваш сайт оплату банковскими картами через платежный сервис TipTop Pay. 
Для корректной работы модуля необходима регистрация в сервисе.

Порядок регистрации описан в [документации TipTop Pay](https://tiptoppay.kz/#connect)

### Совместимость:
- HikaShop >= 3.4.0
- Joomla >= 3.8.2

Модуль тестировался на HikaShop = 3.4.0 (Joomla = 3.8.2)

### Возможности:
- Одностадийная схема оплаты;
- Двухстадийная схема оплаты;
- Поддержка онлайн-касс;
- Отправка чеков по email;
- Отправка чеков по SMS;

### Установка через панель управления
В панели адмниистратора зайти в раздел `Расширения` -> `Менеджер расширений` -> `Установка`

После чего загрузите zip архив. Зайдите в управление плагинами и убедитесь, что плагин включен.

### Настройка плагина
- Перейдите в настройки HikaShop  
- Далее необходимо перейти в раздел `Система` -> `Способы оплаты` -> `Создать`   
- Выбрать  платежный плагин TipTop Pay
- В качестве имени новой платежной системы указать: TipTop Pay - **это обязательный пункт**  
- Выставить настройку `Опубликовано` - `да`
- Далее ввести Public ID - из [личного кабинета](https://merchant.tiptoppay.kz) TipTop Pay и пароль API, указываем нужные статусы для реагирования плагина на изменения заказа. Для двухстадийных оплат необходимо добавить новый статус.

После настройки плагина - сохраните изменения и перейдите к настройки вебхуков в ЛК TipTop Pay
Все подсказки по настройки есть в шапке плагина. 

### Настройка вебхуков

В [личном кабинете](https://merchant.tiptoppay.kz) TipTop Pay в настройках вашего сайта вставьте следующие URL для коректной работы модуля:

* **Check**  
`https://domain.kz/index.php?option=com_hikashop&ctrl=checkout&task=notify&if_payment=tiptoppay&tmpl=component&action=check`
* **Pay**  
`https://domain.kz/index.php?option=com_hikashop&ctrl=checkout&task=notify&if_payment=tiptoppay&tmpl=component&action=pay`
* **Fail**  
`https://domain.kz/index.php?option=com_hikashop&ctrl=checkout&task=notify&if_payment=tiptoppay&tmpl=component&action=fail`
* **Cancel**  
`https://domain.kz/index.php?option=com_hikashop&ctrl=checkout&task=notify&if_payment=tiptoppay&tmpl=component&action=cancel`
* **Confirm**  
`https://domain.kz/index.php?option=com_hikashop&ctrl=checkout&task=notify&if_payment=tiptoppay&tmpl=component&action=confirm`
* **Refund**  
`https://domain.kz/index.php?option=com_hikashop&ctrl=checkout&task=notify&if_payment=tiptoppay&tmpl=component&action=refund`

Где **domain.kz** - адрес сайта.

### Changelog

**1.0.0**
- Публикация модуля